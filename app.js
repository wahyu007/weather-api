const fetch = require('node-fetch');

fetch(
    "https://api.openweathermap.org/data/2.5/weather?q=mumbai&units=metric&APPID=YOUR-KEY"
)
.then((response) => response.json())
.then((data) => console.log(data))
.catch((err) => console.log(err))
